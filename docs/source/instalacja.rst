Instrukcje uruchomienia aplikacji za pomocą docker-compose
=================================================================
Wymagane instalacje
----------------------

Należy zainstalować Docker oraz Docker compose

Instrukcje krok po kroku
---------------------------

- Znajdź plik *DockerFile* (bez żadnego rozszerzenia) w nadrzędnym folderze projektu.
Poniżej przykład:


.. code-block:: java

    FROM  openjdk:8-jdk-alpine
    ADD target/IOSpringData-0.0.1-SNAPSHOT.jar .
    EXPOSE 8000
    CMD java -jar IOSpringData-0.0.1-SNAPSHOT.jar


- Znajdź skrypt, który będzie inicjalizować bazę danych (o rozszerzeniu *.sh*). Znajduje się w katalogu docker-entrypoint-initdb.d.



- Następnie odszukaj plik *docker-compose.yaml*. Zwróć uwagę na to, że pierwszy z dodanych portów to ten, na którym nasłuchuje kontener. Przykład pliku *docker-compose*:


.. code-block:: java

  version: "3"
  services:
      app:
          image: "nazwa_obrazu"
          build:
              context: .
              dockerfile: "Dockerfile"
          environment:
              POSTGRES_USER: ${POSTGRES_USER}
              POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
          ports:
              - 8000:8008
      db:
          image: postgres:latest
          volumes:
              - "./docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d"
          environment:
              POSTGRES_USER: ${POSTGRES_USER}
              POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
          ports:
              - ${DB_PORT}:5432

- Sprawdź czy w projekcie jest plik *.env*:


.. code-block:: java

  POSTGRES_USER=postgres
  POSTGRES_PASSWORD=mysecretpass
  DB_PORT=5433


- Pamiętaj o odpowiedniej konfigufacji bazy danych i portów w *application.properties*. Sprawdź czy porty się zgadzają:


.. code-block:: java

  server.port=8008
  spring.datasource.url=jdbc:postgresql://db:5432/docker
  spring.datasource.username=${POSTGRES_USER}
  spring.datasource.password=${POSTGRES_PASSWORD}
  spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.PostgreSQLDialect
  spring.jpa.hibernate.ddl-auto = update

- Budowanie obrazu odbywa się komendą:


``docker-compose build``


- Następnie uruchamiamy bazę danych i aplikację (razem):


``docker-compose up``


- Można też uruchomić tylko bazę lub aplikację, na przykład:


``docker-compose up db``


uruchamia tylko bazę określoną jako *db* w pliku *.yaml*.




