Diagramy
======================
Diagram przypadków użycia
--------------------------
Poniżej przedstawiono wybrane przypadki użycia dla roli zalogowanego użytkownika.


.. uml::

   @startuml
   left to right direction
   actor "ROLE_CUSTOMER" as c

   usecase (Pobiera listę produktów) as (Pob)
   usecase (Pobiera produkt o zadanym id) as (Pobid)
   usecase (Dodaje nowe zamówienie do bazy) as (Dod)

   c --> Pob
   c --> Pobid
   c --> Dod
   @enduml


Diagram klas
--------------

.. uml::

   @startuml
   skinparam classAttributeIconSize 0
   class Product{
   {field}-id:Long
   {field}-name:String
   {field}-price:float
   {field}-avaliable:boolean

   {method}+getters
   {method}+setters
   }

   class Order{
      {field}-id:Long
      {field}-customer:Customer
      {field}-products:Set<Products>
      {field}-placeDate:LocalDateTime
      {field}-status:String

      {method}+getters
      {method}+setters
      }

   class Customer{
         {field}-id:Long
         {field}-name:String
         {field}-address:String

         {method}+getters
         {method}+setters
         }

   interface ProductRepo
   interface OrderRepo
   interface CustomerRepo

   class ProductManager{
            {field}-ProductRepo:ProductRepo

            {method}+findAllById
            {method}+findAll
            {method}+save
            {method}+saveFromPatch
            {method}+fillDB
            }

   class OrderManager{
               {field}-OrderRepo:OrderRepo

               {method}+findAllById
               {method}+findAll
               {method}+save
               {method}+saveFromPatch
               {method}+fillDB
               }

   class CustomerManager{
               {field}-CustomerRepo:CustomerRepo

               {method}+findAllById
               {method}+findAll
               {method}+save
               {method}+saveFromPatch
               {method}+fillDB
               }

   class ProductApi{
                  {field}-products:ProductManager

                  {method}+getAll
                  {method}+getById
                  {method}+addProduct
                  {method}+updateProduct
                  {method}+patchProduct
                  }

   class OrderApi{
                     {field}-orders:OrderManager

                     {method}+getByIdOrders
                     {method}+getAllOrders
                     {method}+addOrder
                     {method}+updateOrder
                     {method}+patchOrder
                     }

   class CustomerApi{
                     {field}-customers:CustomerManager

                     {method}+getAllCustomers
                     {method}+getByIdCustomer
                     {method}+addCustomer
                     {method}+updateCustomer
                     {method}+patchCustomer
                     }

   ProductApi --> ProductManager
   OrderApi --> OrderManager
   CustomerApi --> CustomerManager

   ProductManager --> ProductRepo
   OrderManager --> OrderRepo
   CustomerManager --> CustomerRepo

   ProductRepo --> Product
   OrderRepo --> Order
   CustomerRepo --> Customer

   Product *-- Order
   Order -- Customer




   @enduml
