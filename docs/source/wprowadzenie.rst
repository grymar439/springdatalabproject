Docker
===========================
Czym jest docker?
-------------------------
Docker to narzędzie, które pozwala uruchamiać kontenery, dzięki którym możemy
uruchomić dodatkowy, odizolowany system operacyjny z gotową aplikacją. Oznacza
to tyle, że aplikacja działająca w kontenerze ma zdefiniowane zależności
i biblioteki konieczne do jej poprawnego działania.


Czym jest docker-compose?
----------------------------------
Docker-compose to narzędzie, które automatyzuje tworzenie i uruchamianie wielu
kontenerów. Jest zdefiniowany jako plik, w którym występują wszystkie
następujące po sobie czynności, tj. zasady współpracy konenerów. Widać to np. na
przykładzie współpracy aplikacji z bazą danych.
