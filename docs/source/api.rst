REST api w projekcie
========================

Endpointy
-----------------------

W projekcie zostały wprowadzone role:

   **ROLE_CUSTOMER** - zalogowany użytkownik posiadający uprawnienia klienta
   **ROLE_ADMIN** - administrator


Na ich podstawie zostały podzielone endpointy w projekcie.


**Uprawnienia administratora:**

   * POST .../api/admin/product - dodaje nowy produkt do bazy

   * PUT .../api/admin/product - modyfikuje istniejący produkt o zadanym id,

   * PUT …/api/admin/product - modufikuje istniejący produkt o zadanym id,

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   * PATCH …/api/admin/product - częściowo modyfikuje istniejący produkt o zadanym id,

   * POST …/api/admin/customer - umieszcza nowego klienta w bazie,


   * PUT …/api/admin/customer - modufikuje istniejącego klienta o zadanym id,

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   * PATCH …/api/admin/customer - częściowo modyfikuje istniejącego klienta o podanym id,

   * PUT …/api/admin/order - modufikuje istniejące zamówienie o zadanym id,

   * PATCH …/api/admin/order - częściowo modyfikuje istniejące zamówienie o podanym id.



**Uprawnienia zarówno zalogowanego użytkownika jak i administratora:**

   * GET …/api/product - zwraca produkt o odpowiednim id,

   * GET …/api/product/all - zwraca listę wszystkich produktów,

^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   * GET …/api/order - zwraca zamówienie o podanym id,

   * GET ../api/order/all - zwraza listę wszystkich zamówień,

   * POST …/api/order - umieszcza nowe zamówienie w bazie,



**Uprawnienia tylko dla zalogowanego użytkownika:**

   * GET …/api/customer - zwraca klienta o podanym id,

   * GET …/api/customer/all - zwraca listę wszystkich klientów,
