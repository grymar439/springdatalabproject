package edu.ib.iospringdata.api;
import edu.ib.iospringdata.dao.entity.Order;
import edu.ib.iospringdata.dao.entity.Product;
import edu.ib.iospringdata.manager.OrderManager;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/")
public class OrderApi {
    private OrderManager orders;

    public OrderApi(OrderManager orders) {
        this.orders = orders;
    }

    @GetMapping("/order/all")
    public Iterable<Order> getAllOrders(){
        return orders.findAll();
    }


    @GetMapping("/order")
    public Optional<Order> getByIdOrder(@RequestParam Long id){
        return  orders.findById(id);

    }

    @PostMapping("/order")
    public Order addOrder(@RequestBody Order order){
        Set<Product> products = new HashSet<>();
        for(Product p: order.getProducts())
            products.add(p);
        order.setCustomer(order.getCustomer());
        order.setProducts(products);
        order.setStatus(order.getStatus());
        order.setPlaceDate(order.getPlaceDate());
        return orders.save(order);

    }
    @PutMapping("/admin/order")
    public Order updateOrder(@RequestBody Order order){
        order.setId(order.getId());
        return orders.save(order);

    }
    @PatchMapping("/admin/order")
    public void patchOrder(@RequestParam Long id, @RequestBody Order order){
        orders.saveFromPatch(order, id);

    }
}
