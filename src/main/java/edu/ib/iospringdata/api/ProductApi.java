package edu.ib.iospringdata.api;


import edu.ib.iospringdata.dao.entity.Product;
import edu.ib.iospringdata.manager.ProductManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

@RestController
@RequestMapping("/api/")
public class ProductApi {

    private ProductManager products;

    @Autowired
    public ProductApi(ProductManager products) {
        this.products = products;
    }

    @GetMapping("/product/all")
    public Iterable<Product> getAll(){
        return products.findAll();
    }


    @GetMapping("/product")
    public Optional<Product> getById(@RequestParam Long id){
        return  products.findById(id);

    }

    @PostMapping("/admin/product")
    public Product addProduct( @RequestBody Product product){
        return products.save(product);

    }
    @PutMapping("/admin/product")
    public Product updateProduct(@RequestBody Product product){
        return products.save(product);

    }
    @PatchMapping("/admin/product")
    public void patchProduct(@RequestParam Long id, @RequestBody Product product){
        products.saveFromPatch(product, id);

    }
}
