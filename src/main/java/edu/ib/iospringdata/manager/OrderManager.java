package edu.ib.iospringdata.manager;

import edu.ib.iospringdata.dao.OrderRepo;
import edu.ib.iospringdata.dao.entity.Customer;
import edu.ib.iospringdata.dao.entity.Order;
import edu.ib.iospringdata.dao.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class OrderManager {
    private OrderRepo orderRepo;

    @Autowired
    public OrderManager(OrderRepo orderRepo) {
        this.orderRepo = orderRepo;
    }


    public Optional<Order> findById(Long id){
        return orderRepo.findById(id);
    }

    public Iterable<Order> findAll(){
        return orderRepo.findAll();
    }

    public Order save(Order order){
        return orderRepo.save(order);
    }

   public void saveFromPatch(Order order, Long id){
        Order order1 = orderRepo.findById(id).get();
        boolean needUpdate = false;
        if(order.getCustomer()!=null){
            order1.setCustomer(order.getCustomer());
            needUpdate = true;
        }
        if(order.getProducts()!=null){
            order1.setProducts(order.getProducts());
            needUpdate = true;
        }
        if(order.getPlaceDate()!=null){
            order1.setPlaceDate(order.getPlaceDate());
            needUpdate  = true;
        }
        if(StringUtils.hasLength(order.getStatus())){
            order1.setStatus(order.getStatus());
            needUpdate = true;
        }
        if(needUpdate){
            orderRepo.save(order1);
        }
   }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){
        Product p1 = new Product("maslo",15.5f,true);
        Product p2 = new Product("Kustosz Mocny",1.5f,false);
        Set<Product> products = new HashSet<>();
        products.add(p1);
        products.add(p2);
        save( new Order(new Customer("Anna Nowak", "Zabrze"),products, LocalDateTime.now(),"in progress"));
    }
}
