package edu.ib.iospringdata.manager;

import edu.ib.iospringdata.dao.CustomerRepo;
import edu.ib.iospringdata.dao.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


import java.util.Optional;


@Service
public class CustomerManager {

    CustomerRepo customerRepo;

    @Autowired
    public CustomerManager(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    public Optional<Customer> findById(Long id){
        return customerRepo.findById(id);
    }

    public Iterable<Customer> findAll(){
            return customerRepo.findAll();
    }

    public Customer save(Customer customer){
        return customerRepo.save(customer);
    }

    public void saveFromPatch(Customer customer, Long id){
        Customer customer1 = customerRepo.findById(id).get();
        boolean needUpdate = false;
        if(StringUtils.hasLength(customer.getName())){
            customer1.setName(customer.getName());
            needUpdate = true;
        }
        if(StringUtils.hasLength(customer.getAddress())){
            customer1.setAddress(customer.getAddress());
            needUpdate = true;
        }

        if(needUpdate){
            customerRepo.save(customer1);
        }
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){
        save(new Customer("Karol Kijowski", "Wroclaw"));
        save(new Customer("Michał Kmin", "Lublin"));

    }

}
