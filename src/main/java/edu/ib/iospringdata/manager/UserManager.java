package edu.ib.iospringdata.manager;


import edu.ib.iospringdata.UserDtoBuilder;
import edu.ib.iospringdata.dao.UserRepo;
import edu.ib.iospringdata.dao.entity.User;
import edu.ib.iospringdata.dao.entity.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class UserManager {

    private UserRepo userRepo;

    @Autowired
    public UserManager(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public UserDto findByName(String name){
        return userRepo.getUserDtoByName(name);
    }

    public UserDto save(UserDto user){
        return userRepo.save(user);
    }


    @EventListener(ApplicationReadyEvent.class)
    public void fillDB(){
        User user1 = new User("user_first","user1","ROLE_CUSTOMER");
        User user2 = new User("admin","admin1","ROLE_ADMIN");
        UserDtoBuilder userDtoBuilder1 = new UserDtoBuilder(user1);
        UserDto userDto1 = userDtoBuilder1.getUserDto();
        UserDtoBuilder userDtoBuilder2 = new UserDtoBuilder(user2);
        UserDto userDto2 = userDtoBuilder2.getUserDto();
        userRepo.save(userDto1);
        userRepo.save(userDto2);

    }
}
