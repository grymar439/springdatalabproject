package edu.ib.iospringdata;


import edu.ib.iospringdata.dao.CustomerRepo;
import edu.ib.iospringdata.dao.OrderRepo;
import edu.ib.iospringdata.dao.ProductRepo;
import edu.ib.iospringdata.dao.entity.Customer;
import edu.ib.iospringdata.dao.entity.Order;
import edu.ib.iospringdata.dao.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DBMockData {
//    private ProductRepo productRepo;
//    private CustomerRepo customerRepo;
//    private OrderRepo orderRepo;
//
//    @Autowired
//    public DBMockData(ProductRepo productRepo, CustomerRepo customerRepo, OrderRepo orderRepo) {
//        this.productRepo = productRepo;
//        this.customerRepo = customerRepo;
//        this.orderRepo = orderRepo;
//    }
//
//    @EventListener(ApplicationReadyEvent.class)
//    public void fil(){
//        Product p1 = new Product("jaja",9.5f,true);
//        Product p2 = new Product("cukier",50.5f,false);
//        Customer c1 = new Customer("Anna Nowak", "Zabrze");
//        Set<Product> products = new HashSet<>();
//        products.add(p1);
//        products.add(p2);
//        Order order = new Order(c1,products, LocalDateTime.now(),"in progress");
//
//        productRepo.save(p1);
//        productRepo.save(p2);
//        customerRepo.save(c1);
//        orderRepo.save(order);
//
//
//    }
}
