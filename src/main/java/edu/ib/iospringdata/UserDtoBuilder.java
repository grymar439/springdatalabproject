package edu.ib.iospringdata;

import edu.ib.iospringdata.dao.entity.User;
import edu.ib.iospringdata.dao.entity.UserDto;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserDtoBuilder {
    private String name;
    private String passwordHash;
    private String role;
    private User user;

    public UserDtoBuilder(User user){
        this.user = user;
        this.name = user.getName();
        this.role = user.getRole();
        this.passwordHash = getUserDto().getPasswordHash();
    }

    public UserDto getUserDto(){
        return new UserDto(name, getEncodedPass(), role);

    }

    public String getEncodedPass(){
        PasswordEncoderConfig passwordEncoderConfig = new PasswordEncoderConfig();
        String password = user.getPassword();
        PasswordEncoder passwordEncoder = passwordEncoderConfig.passwordEncoder();
        passwordHash = passwordEncoder.encode(password);
        return passwordHash;
    }
}
